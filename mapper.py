#!/usr/bin/env python

import sys
import io
import re
import nltk
from nltk.corpus import stopwords

nltk.download('stopwords', quiet=True)

punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
stop_words = set(stopwords.words('english'))

input_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='latin1')

for line in input_stream:
    # Extracting the third column from CSV
    line = line.split(',', 3)[2]
    line = line.strip()

    # Remove excess whitespace, transform to lower case
    line = re.sub(r'[^\w\s]', '', line)
    line = line.lower()
    line = ' '.join(line.split())

    for x in line:
        if x in punctuations:
            line = line.replace(x, " ")

    words = line.split()
    for word in words:
        if word.isalpha() and word not in stop_words:
            print('%s\t%s' % (word, 1))
