import csv
import math
import pandas as pd
from collections import Counter

def load_preprocessed_text_from_file(file_path):
    with open(file_path, 'r', encoding='utf-8') as text_file:
        preprocessed_text = [line.strip() for line in text_file]
    return preprocessed_text

# Replace 'path/to/your/preprocessed/vocabulary/file.txt' with the actual path to your preprocessed vocabulary file
preprocessed_vocab_file_path = '/content/reducer_output.txt'
preprocessed_vocab = load_preprocessed_text_from_file(preprocessed_vocab_file_path)

# Replace 'path/to/your/documents/file.csv' with the actual path to your CSV file containing original documents
documents_file_path = '/content/train.csv'

def load_documents_from_csv(file_path):
    df = pd.read_csv(file_path)
    last_column = df.iloc[:, -1].tolist()
    return last_column

# Load the original documents from the CSV file
documents = load_documents_from_csv(documents_file_path)

def calculate_jelinek_mercer(query_tokens, document_tokens, vocab, lambda_value=0.5):
    document_length = len(document_tokens)
    
    # Calculate document term frequencies
    document_tf = Counter(document_tokens)
    
    # Calculate smoothed term probabilities using Jelinek-Mercer smoothing
    probabilities = {}
    
    for term in set(query_tokens):
        term_frequency = document_tf[term] if term in document_tf else 0
        background_probability = vocab[term] / len(vocab) if term in vocab else 0
        smoothed_probability = (lambda_value * term_frequency / document_length) + ((1 - lambda_value) * background_probability)
        probabilities[term] = smoothed_probability
    
    return probabilities

def rank_documents(query, documents, vocab, lambda_value=0.5, top_k=5):
    query_tokens = query.split()  # Assuming each line in the file is a space-separated string
    ranked_documents = []
    
    for i, document in enumerate(documents):
        document_tokens = document.split()  # Assuming each line in the file is a space-separated string
        document_score = sum(calculate_jelinek_mercer(query_tokens, document_tokens, vocab, lambda_value).values())
        ranked_documents.append((i, document_score))
    
    # Sort documents by score in descending order and select the top k
    ranked_documents = sorted(ranked_documents, key=lambda x: x[1], reverse=True)[:top_k]
    
    return ranked_documents

# Test the implementation
queries = ["olympic gold athens", "reuters stocks friday", "investment market prices"]  # Assuming the queries are indices corresponding to documents in the preprocessed file

# Assuming vocab is a Counter object representing the term frequencies in your preprocessed vocabulary
vocab = Counter(preprocessed_vocab)

for query in queries:
    print(f"Query: {query}")
    ranked_results = rank_documents(query, documents, vocab)
    for rank, score in ranked_results:
        print(f"Document {rank + 1}: Score = {score}")
    print("\n")
