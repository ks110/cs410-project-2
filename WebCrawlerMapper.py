#!/usr/bin/env python

import os
import sys
from bs4 import BeautifulSoup
import string

# Function to extract text from HTML content
def extract_text(html_content):
    soup = BeautifulSoup(html_content, 'html.parser')
    return ' '.join(soup.stripped_strings)

# Function to clean and tokenize the text
def clean_and_tokenize(text):
    translator = str.maketrans('', '', string.punctuation)
    cleaned_text = text.translate(translator).lower()
    tokens = cleaned_text.split()
    return tokens

# Set the current directory
current_directory = '/content/downloaded_pages'

# Process each HTML file in the current directory
for filename in os.listdir(current_directory):
    filepath = os.path.join(current_directory, filename)

    if os.path.isfile(filepath) and filepath.endswith(".html"):
        try:
            # Read the content of the HTML file
            with open(filepath, 'r', encoding='utf-8') as file:
                content = file.read()

            # Extract text from HTML content
            text = extract_text(content)

            # Clean and tokenize the text
            tokens = clean_and_tokenize(text)

            # Emit word-count pairs
            for token in tokens:
                print(f"{token}\t1")

        except Exception as e:
            # Print to stderr for debugging
            print(f"Error processing file: {filename}", file=sys.stderr)
            print(str(e), file=sys.stderr)
