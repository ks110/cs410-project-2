import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from rank_bm25 import BM25Okapi

# Part 1: Creating Vocabulary
input_file = "/content/reducer_output.txt"

# Read the file and extract word frequencies
with open(input_file, 'r') as file:
    lines = file.readlines()

# Create a list of tuples (word, frequency) from the file
word_frequencies = [line.strip().split() for line in lines]

# Sort the list based on frequency in descending order
sorted_word_frequencies = sorted(word_frequencies, key=lambda x: int(x[1]), reverse=True)

# Take the top 200 words
top_words = [word for word, frequency in sorted_word_frequencies[:200]]

# Part 2: Computing Word Relevances
# Read documents from CSV file
csv_file_path = "train.csv"
df = pd.read_csv(csv_file_path)

# Combine documents and queries for TF-IDF vectorization
corpus = df.iloc[:, -1].tolist()  # Use all documents for TF-IDF
corpus += [" ".join(top_words)]  # Include the top words as a single document for TF-IDF

# Create TF-IDF vectorizer with adjusted parameters
vectorizer = TfidfVectorizer(smooth_idf=True, sublinear_tf=True)

# Fit and transform the corpus
tfidf_matrix = vectorizer.fit_transform(corpus)
queries = ["olympic gold athens", "reuters stocks friday", "investment market prices"]
# Process each query
for i, query in enumerate(queries):
    # Vectorize the query
    query_vector = vectorizer.transform([query])

    # Compute cosine similarity for TF-IDF
    tfidf_scores = cosine_similarity(query_vector, tfidf_matrix).flatten()

    # Create BM25 model with adjusted parameters for all documents
    tokenized_corpus = [doc.split() for doc in corpus]
    bm25 = BM25Okapi(tokenized_corpus, k1=1.5, b=0.75)

    # Compute BM25 scores for all documents
    bm25_scores = bm25.get_scores(query.split())

    # Ensure both scores have the same length
    min_length = min(len(tfidf_scores), len(bm25_scores))
    tfidf_scores = tfidf_scores[:min_length]
    bm25_scores = bm25_scores[:min_length]

    # Combine scores using a simple linear combination
    alpha = 0.9 # You can adjust this weight based on your preference
    combined_scores = alpha * tfidf_scores + (1 - alpha) * bm25_scores

    # Rank all documents based on the combined scores
    combined_ranked_indices = combined_scores.argsort()[::-1]

    # Print the top 5 relevant documents for the query
    print(f"Top 5 Relevant Documents for Query {i + 1} ({query}):")
    for rank, idx in enumerate(combined_ranked_indices[:5]):
        document_id = idx  # Use the index as a placeholder for document ID
        document_content = corpus[idx]

        print(f"{rank + 1}. Document ID: {document_id} - Score: {combined_scores[idx]}")
        print(f"   Content: {document_content}")
        print()
