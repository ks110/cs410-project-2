Problem 2 files
1. Mapper.py
2. Reducer.py
3. O/P: reducer_output.txt

Problem 3:
1. bm25.py
2. OP: here, newtfidf.txt, in the .ipynb is /content/problem2.txt when executing

Problem 4:
1. jm25.py
2. OP: here, some.txt in the .ipynb is /content/problem3.txt when executing

Problem 5
1. Crawl.py
2. WebCrawlerMapper.py
3. WebCrawlerReducer.py
4. query.py
5. OP: problemlast.txt
6. problem4Vocab.txt - mapper OP
7. finalproblem4vocab.txt - reducer op

Run the .ipynb file in the folder, there are wget commands that fetch the files from github and upload.
