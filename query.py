import math

def read_top_documents(file_path, query, top_n=5):
    with open(file_path, 'r', encoding='utf-8') as file:
        lines = file.readlines()

    # Calculate BM25 score for each document and sort by score
    documents = sorted(lines, key=lambda x: bm25(x.strip().split('\t')[0], query), reverse=True)

    # Take the top N documents
    documents = documents[:top_n]

    return documents

def bm25(doc, query, k1=1.5, b=0.75):
    avgdl = len(doc)
    tf = doc.count(query)
    idf = math.log((len(doc) - doc.count(query) + 0.5) / (doc.count(query) + 0.5) + 1)
    score = idf * (tf * (k1 + 1)) / (tf + k1 * (1 - b + b * len(doc) / avgdl))

    return score

# Example usage
file_path = '/content/FinalProblem4Vocab.txt'
queries = ["university", "ranking", "is"]

for query in queries:
    print(f"\nQuery: {query}")
    
    documents = read_top_documents(file_path, query, top_n=5)

    for i, document in enumerate(documents, start=1):
        score = bm25(document.strip().split('\t')[0], query)
        print("BM25 Score for Document {}: {} - Document: {}".format(i, score, document.strip().split('\t')[0]))
