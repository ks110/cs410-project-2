import requests
import os

def download_page(url, filename):
    response = requests.get(url)
    if response.status_code == 200:
        with open(filename, 'w', encoding='utf-8') as file:
            file.write(response.text)
    else:
        print(f"Failed to download {url}. Status code: {response.status_code}")

# Replace these URLs with the ones you want to crawl
target_urls = [
    "https://en.wikipedia.org/wiki/University_of_Illinois_Urbana-Champaign",
    "https://en.wikipedia.org/wiki/University_of_Wisconsin-Madison",
    "https://en.wikipedia.org/wiki/College_of_Engineering,_Guindy",
    "https://en.wikipedia.org/wiki/Carnegie_Mellon_University",
    "https://en.wikipedia.org/wiki/North_Carolina_State_University",
    "https://en.wikipedia.org/wiki/University_of_Southern_California",
    "https://en.wikipedia.org/wiki/Georgia_Tech",
    "https://en.wikipedia.org/wiki/University_of_California,_Berkeley",
    "https://en.wikipedia.org/wiki/University_of_Michigan",
    "https://en.wikipedia.org/wiki/San_Jose_State_University"
    # Add more URLs as needed
]

folder_name = 'downloaded_pages'
os.makedirs(folder_name, exist_ok=True)

for i, url in enumerate(target_urls):
    filename = os.path.join(folder_name, f"page_{i + 1}.html")
    download_page(url, filename)
    print(f"Page {i + 1} downloaded and saved as {filename}")
